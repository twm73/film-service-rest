package rest;

import domain.Actor;
import domain.Comment;
import domain.Film;
import domain.services.ActorsService;
import domain.services.CommentService;
import domain.services.FilmService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/")
public class FilmsResources {
    private ActorsService actorsDB = new ActorsService();
    private CommentService commentDB = new CommentService();
    private FilmService filmsDB = new FilmService();


    /* pobranie listy filmów */
    @GET
    @Path("/films")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Film> getAllFilms(){return filmsDB.getAll();}

    /* pobranie filmu o podanym id */
    @GET
    @Path("/films/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilmById(@PathParam("id") int id){
        Film result = filmsDB.getById(id);
        if(result==null){return Response.status(404).build();}
        return Response.ok(result).build();
    }

    /* dodanie filmu */
    @POST
    @Path("/films")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addFilm(Film film){
        filmsDB.addFilm(film);
        return Response.ok().build();
    }

    /* aktualizacja filmu o podanym id */
    @PUT
    @Path("/films/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response update(@PathParam("id") int id, Film f){
        Film result = filmsDB.getById(id);
        if(result==null){return Response.status(404).build();}
        f.setId(id);
        filmsDB.update(f);
        return Response.ok().build();
    }

    /* pobranie listy komentarzy ze wszystkich filmów */
    @GET
    @Path("/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comment> getAllComments(){return commentDB.getAll();}

    /* wyświetlenie komentarzy z filmu o podanym id */
    @GET
    @Path("/films/{id}/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCommentsFromFilmById(@PathParam("id") int id){
        Film film = filmsDB.getById(id);
        if(film==null){return Response.status(404).build();}
        List<Comment> result = film.getComments();
        return Response.ok(result).build();
    }

    /* dodanie komentarza do filmu o podanym id */
    @POST
    @Path("/films/{id}/comments")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCommentToFilmById(@PathParam("id") int id, Comment comment){
        Film film = filmsDB.getById(id);
        if(film==null){return Response.status(404).build();}
        int newCommentId = commentDB.addComment(comment);
        film.addComment(commentDB.getById(newCommentId));
        return Response.ok().build();
    }

    /* usunięcie komentarza o wybranym id z filmu o podanym id */
    @DELETE
    @Path("/films/{id}/comments/{commentId}")
    public Response delCommentByIdFromFilmById(@PathParam("id") int id, @PathParam("commentId") int commentId){
        Comment comment = filmsDB.getCommentByIdFromFilmById(id, commentId);
        if(comment==null){return Response.status(404).build();}
        commentDB.delete(comment);
        Film film = filmsDB.getById(id);
        film.removeComment(comment);
        return Response.ok().build();
    }

    /* dodanie podanej oceny do filmu o podanym id */
    @POST
    @Path("/films/{id}/rating")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addRatingToFilmById(@PathParam("id") int id, int rating){
        Film film = filmsDB.getById(id);
        if(film==null){return Response.status(404).build();}
        film.updateRating(rating);
        return Response.ok().build();
    }

    /* dodanie aktora */
    @POST
    @Path("/actors")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addActor(Actor actor){
        actorsDB.addActor(actor);
        return Response.ok().build();
    }

    /* pobranie listy aktorów */
    @GET
    @Path("/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Actor> getAllactors(){return actorsDB.getAll();}

    /* dodanie aktora do filmu o podanym id */
    @POST
    @Path("/films/{id}/actors")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addActorToFilmById(@PathParam("id") int id, Actor a){
        Film film = filmsDB.getById(id);
        if(film==null){return Response.status(404).build();}
        int actorId = actorsDB.addActor(a);
        film.addActor(actorsDB.getById(actorId));
        return Response.ok().build();
    }

    /* dodanie aktora o podanym id do filmu o podanym id */
    @POST
    @Path("/films/{id}/actors/{actorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addActorToFilmById(@PathParam("id") int id, @PathParam("actorId") int actorId){
        Film film = filmsDB.getById(id);
        if(film==null){return Response.status(404).build();}
        Actor actor = actorsDB.getById(actorId);
        if(actor==null){return Response.status(404).build();}
        film.addActor(actorsDB.getById(actorId));
        return Response.ok().build();
    }

    /* pobranie aktorów z filmu o podanym id */
    @GET
    @Path("/films/{id}/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getActorsFromFilmById(@PathParam("id") int id){
        Film film = filmsDB.getById(id);
        if(film==null){return Response.status(404).build();}
        List<Actor> result = film.getActors();
        if(result==null){return Response.status(404).build();}
        return Response.ok(result).build();
    }

    /* pobranie filmów z aktorem o podanym id */
    @GET
    @Path("/actors/{id}/films")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilmsWithActorById(@PathParam("id") int id){
        List<Film> result = filmsDB.getByActor(id);
        return Response.ok(result).build();
    }


}
