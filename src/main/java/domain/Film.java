package domain;

import java.util.List;

public class Film {
    private int id;
    private String title;
    private float rating;
    private List<Comment> comments;
    private List<Actor> actors;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void updateRating(int newRating){
        if (this.rating == 0) {this.rating = newRating;}
        else {this.rating = (this.rating + newRating)/2;}
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment){
        this.comments.add(comment);
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public void addActor(Actor actor){
        this.actors.add(actor);
    }

    public void removeActor(Actor actor){
        this.actors.remove(actor);
    }

    public void removeComment(Comment comment){
        this.comments.remove(comment);
    }

}
