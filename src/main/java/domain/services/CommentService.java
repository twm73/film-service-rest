package domain.services;

import domain.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentService {
    private static List<Comment> db = new ArrayList<Comment>();
    private static int currentId = 0;

    public List<Comment> getAll(){
        return db;
    }

    public Comment getById(int id){
        for(Comment c : db){
            if(c.getId()==id){return c;}
        }
        return null;
    }

    public int addComment (Comment c){
        c.setId(++currentId);
        db.add(c);
        return c.getId();
    }

    public void update (Comment comment){
        for(Comment c : db){
            if(c.getId()==comment.getId()){
                c.setText(comment.getText());
            }
        }
    }

    public void delete(Comment c){
        db.remove(c);
    }
}
