package domain.services;

import domain.Actor;
import domain.Comment;
import domain.Film;

import java.util.ArrayList;
import java.util.List;

public class FilmService {
    private static List<Film> db = new ArrayList<Film>();
    private static int currentId = 0;

    public List<Film> getAll(){return db;}

    public Film getById(int id){
        for(Film f : db){
            if(f.getId()==id){return f;}
        }
        return null;
    }

    public int addFilm(Film f){
        f.setId(++currentId);
        db.add(f);
        return f.getId();
    }

    public List<Film> getByActor(int id){
        List<Film> filmsByActor = new ArrayList<Film>();
        for (Film f : db){
            for (Actor a : f.getActors()){
                if(a.getId()==id){filmsByActor.add(f);}
            }
        }
        return filmsByActor;
    }

    public void update (Film film){
        for (Film f : db){
            if(f.getId()==film.getId()){
                f.setActors(film.getActors());
                f.setComments(film.getComments());
                f.setTitle(film.getTitle());
                f.setRating(film.getRating());
            }
        }
    }

    public void updateRating (int id, int rating){
        for(Film f : db){
            if(f.getId()==id){
                f.updateRating(rating);
            }
        }
    }

    public Comment getCommentByIdFromFilmById(int filmId, int commentId){
        for(Film f : db){
            if(f.getId()==filmId){
                for(Comment c : f.getComments()){
                    if(c.getId()== commentId){return c;}
                }
                return null;
            }
        }
        return null;
    }
}
