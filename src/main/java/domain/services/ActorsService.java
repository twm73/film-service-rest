package domain.services;

import domain.Actor;

import java.util.ArrayList;
import java.util.List;

public class ActorsService {
    private static List<Actor> db = new ArrayList<Actor>();
    private static int currentId = 0;

    public List<Actor>getAll(){return db;}

    public Actor getById(int id){
        for (Actor a : db){
            if(a.getId()==id){return a;}
        }
        return null;
    }

    public int addActor(Actor a){
        a.setId(++currentId);
        db.add(a);
        return a.getId();
    }

    public void update(Actor actor){
        for (Actor a : db){
            if(a.getId()==actor.getId()){
                a.setName(actor.getName());
                a.setSurname(actor.getSurname());
            }
        }
    }
}
